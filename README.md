# nanojson [![pipeline status](https://gitlab.com/tyge/nanojson/badges/master/pipeline.svg)](https://gitlab.com/tyge/nanojson/commits/master) [![coverage report](https://gitlab.com/tyge/nanojson/badges/master/coverage.svg)](https://tyge.gitlab.io/nanojson/) [![GoDoc](https://godoc.org/howl.moe/nanojson?status.svg)](https://godoc.org/howl.moe/nanojson)

> _Parse JSON in nanoseconds, not microseconds_

For anyone who stumbles on this page, possibly while looking for a good alternative JSON parser:

This is an experiment I wrote over december-january 2017-2018. I tried writing a fully-compliant JSON parser basically using an intermediate representation as a custom "Value". I thought this could work, but in spite of all the optimisations I tried implementing, it would still not beat [jsonparser,](https://github.com/buger/jsonparser) which judging from the benchmarks is possibly the fastest JSON parser out there. Originally I intended to complete this project, including a fast encoder, but that did not go very far (there is some encoding code, which works and is used in the project, but no marshalling function).

If you want to try it out, feel free to! But I'll no longer work on the code. There is also a branch called `newstuff` with a partial rewrite I made in May 2020. It attempts to simplify the Value struct and the overall mechanisms, but the benchmarks turned out to be actually slower.

As for my own projects, when I'll need to encode/decode JSON quickly I'll probably use [easyjson.](https://github.com/mailru/easyjson)

If you were interested in this, you might be also interested in a similar project of mine: [howl.moe/binary](https://pkg.go.dev/mod/howl.moe/binary) - or: a 2x faster binary encoder/decoder.

_so long, and thanks for all the fish\
May 24, 2020_

```
$ go test -bench=JSONParser -benchmem -run=^$
goos: linux
goarch: amd64
pkg: howl.moe/nanojson
BenchmarkJSONParserSuite/Small-4         	  620031	      1920 ns/op	  98.43 MB/s	     113 B/op	       6 allocs/op
BenchmarkJSONParserSuiteByJSONParser       	 1000000	      1113 ns/op	 169.82 MB/s	     112 B/op	       6 allocs/op
```
