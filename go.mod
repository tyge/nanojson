module howl.moe/nanojson

go 1.14

require (
	github.com/buger/jsonparser v1.0.0
	github.com/valyala/bytebufferpool v1.0.0
)
